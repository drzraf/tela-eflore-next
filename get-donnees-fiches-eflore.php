<?php
/**
 * @category	PHP 5.2
 * @package	Framework
 * @author	    Raphaël Droz <raphael@tela-botanica.org>
 * @copyright	Copyright (c) 2013, Tela Botanica (accueil@tela-botanica.org)
 * @license	http://www.gnu.org/licenses/gpl.html Licence GNU-GPL-v3
 * @license	http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt Licence CECILL-v2
 */

/*
  - bootstrap le framework
  - bootstrap une fiche
  - puis, pour un ou plusieurs onglet (automatique ou manuellement),
  obtient les données correspondante.

  L'objectif est de pouvoir générer un flux de données exhaustive pour une Fiche donnée
  tout en restant assez indépendant des (lourds) appels à l'API.
  Une finalité est de pouvoir s'orienter vers un cron pour index Sphinx, tout en
  permettant d'identifier les limites de l'API.
*/

define('_DIR_FRAMEWORK', '/home/yug/comp/sources/eflore-framework/framework');
define('_DIR_CONSULT', '/home/yug/comp/sources/eflore-consultation');
define('_DIR_SERVICES', '/home/yug/comp/sources/eflore-projets/services/modules/0.1');

//require_once __DIR__ . '/../../framework.php';
set_include_path(_DIR_FRAMEWORK . PATH_SEPARATOR .
		 _DIR_CONSULT . '/controleurs' . PATH_SEPARATOR .
		 _DIR_CONSULT . '/bibliotheque' . PATH_SEPARATOR .
		 _DIR_CONSULT . '/modules/fiche' . PATH_SEPARATOR .
		 _DIR_CONSULT . '/modules/recherche' . PATH_SEPARATOR . // pour bootstrap de Recheche (défaut)
		 _DIR_CONSULT . '/modules/niveau' . PATH_SEPARATOR . // pour bootstrap de Recheche (défaut)
		 _DIR_CONSULT . '/modules/recherche_simple' . PATH_SEPARATOR . // pour bootstrap de Recheche (défaut)
		 _DIR_CONSULT . '/modules/recherche_avancee' . PATH_SEPARATOR . // pour bootstrap de Recheche (défaut)
		 _DIR_CONSULT . '/metier/api_0.1' . PATH_SEPARATOR .
		 _DIR_SERVICES . '/commun' . PATH_SEPARATOR .
		 _DIR_SERVICES . '/bdtfx' . PATH_SEPARATOR .
		 _DIR_CONSULT . '/modules/fiche/formateurs' . PATH_SEPARATOR .
		 get_include_path());

spl_autoload_extensions('.php');
spl_autoload_register();

/*require_once _DIR_FRAMEWORK . '/Controleur.php';
  require_once _DIR_CONSULT . '/controleurs/AppControleur.php';
  require_once _DIR_CONSULT . '/bibliotheque/AppUrls.php';
  require_once _DIR_CONSULT . '/controleurs/aControleur.php';
  require_once _DIR_CONSULT . '/bibliotheque/Conteneur.php';
  require_once _DIR_CONSULT . '/modules/fiche/Fiche.php';*/


// require_once _DIR_CONSULT . '/modules/fiche/Fiche.php'
require_once('Framework.php');
Framework::setCheminAppli(_DIR_CONSULT . '/');
Framework::setInfoAppli(Config::get('info'));
restore_error_handler(); restore_exception_handler(); error_reporting(E_ALL);
// idéalement
// $a = new Fiche(new Conteneur(array()));


require_once('Config.php');
require_once('Controleur.php');
require_once('aControleur.php');
require_once('AppControleur.php');
require_once('AppUrls.php');
require_once('Conteneur.php');


Config::charger(_DIR_FRAMEWORK . '/config.ini');
Config::charger(_DIR_CONSULT . '/configurations/config.ini');

// mandated ?
require_once('Recherche.php');
require_once('Niveau.php');
require_once('RechercheSimple.php');
require_once('RechercheAvancee.php');
require_once('Eflore.php');
require_once('Wikini.php');
//
AppControleur::initialiser();

require_once('Commun.php');
require_once('CommunNomsTaxons.php');
require_once('Eflore.php');
require_once('Taxons.php');
require_once('Noms.php');

require_once('NomCourant.php');
require_once('Nom.php');


require_once('Fiche.php');
$a = new Fiche();
$_GET['num_nom'] = 141;
$_GET['referentiel'] = 'bdtfx';

$a->initialiser();

$classes = array(
  // 'illustrations',
  // 'repartition',
  // 'ecologie', // fait main (mais peu utile)

  // 'nomenclature', // TODO
  // 'description', // fait main
  // 'ethnobotanique', // fait main
  // 'bibliographie', // fait main
  // 'statut', // TODO
);

// pour nomenclature
require_once('MetaDonnees.php');
require_once('Wikini.php');

// pour description
require_once('Textes.php');
require_once('Informations.php');

// pour ethnobotanique
require_once('NomsVernaculaires.php');

// pour bibliographie
require_once('BiblioBota.php');

// pour statuts
require_once('Statuts.php');

// pour ecologie
require_once('Graphiques.php');
require_once('Syntaxons.php');

// pour repartition
require_once('Cartes.php'); // TODO

// way 1
foreach($classes as $c) {
  $a->onglet = $c;
  $b = $a->obtenirDonnees();
  var_dump($b);die();
}


// non-nécessaire si l'on peut récupérer le conteneur
// initialisé par new Fiche()
// $conteneur = new Conteneur($a->parametres);

// description
error_log(sprintf("== description"));
$onglet = new Description($a->conteneur);
$onglet->obtenirDonnees();
$onglet->getCoste();
echo $onglet->donnees['wikini']['description'];
echo implode('; ', $onglet->donnees['coste']['description']);

// bibliographie
error_log(sprintf("== bibliographie"));
Config::charger(_DIR_CONSULT . '/configurations/bdtfx.ini');
$onglet = new Bibliographie($a->conteneur);
$onglet->obtenirDonnees();
echo implode('; ', $onglet->donnees['flores']['liste_flores']);
echo implode('; ', $onglet->donnees['bibliobota']['references']);

// ethnobota
error_log(sprintf("== ethnobota"));
Config::charger(_DIR_CONSULT . '/configurations/bdtfx.ini');
$onglet = new Ethnobotanique($a->conteneur);
$onglet->obtenirDonnees();
echo implode('; ', array_map(function($v) { return $v['nom_vernaculaire']; }, $onglet->donnees['nvjfl']['noms'])) . '; ' .
  implode('; ', array_map(function($v) { return $v['nom_vernaculaire']; }, $onglet->donnees['nvps']['noms']));

// ecologie
error_log(sprintf("== ecologie"));
Config::charger(_DIR_CONSULT . '/configurations/bdtfx.ini');
$onglet = new Ecologie($a->conteneur);
$onglet->obtenirDonnees(); // slow !
var_dump($onglet->donnees['baseflor']['legende']);


error_log(sprintf("== nomenclature"));
Config::charger(_DIR_CONSULT . '/configurations/bdtfx.ini');
$onglet = new Nomenclature($a->conteneur);
print_r($onglet->obtenirDonnees());



/*
  API: TODO:
  - Chaque service de /consultation/formateur/ doit définir en en-tête:
  * référentiels supportés
  * fichiers/directives de configuration nécessaire
  * class utilisées (namespace "use")

  - obtenirDonnees() doit prendre ses paramètres [optionnels] par argument, sans compter
  sur l'instanciation et la définition d'attributs
  - obtenirDonnees() doit retourner les valeurs générées
  - obtenirDonnees() ne doit pas traiter le formattage des résultats (getBloc() oui)
  - si $this->données reste nécessaire pour une quelconque raison, celui-ci doit être public

  - pour Fiche.php
  - onglets ne doit plus être un attribut (pas même public)
  - executerFiche(), executerOnglet() et obtenirDonnees() doivent prendre un $onglet comme paramètre
  - $parametre et $conteneur doivent être "public"

*/