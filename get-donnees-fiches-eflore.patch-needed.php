Index: controleurs/AppControleur.php
===================================================================
--- controleurs/AppControleur.php	(révision 1137)
+++ controleurs/AppControleur.php	(copie de travail)
@@ -59,8 +59,6 @@
 		self::initialiserRegistre();
 		self::chargerConfigReferentiel();
 
-		spl_autoload_register(array(get_class(), 'chargerClasse'));
-
 		self::executerModule();
 	}
 
@@ -140,21 +138,7 @@
 		return ($url) ? new Url($url) : $url;
 	}
 
-	private static function chargerClasse($nom_classe) {
-		$dossiers_classes = array(
-			Config::get('chemin_modules').self::getNomDossierModuleCourrant().DS,
-			Config::get('chemin_modules').self::getNomDossierDepuisClasse($nom_classe).DS,
-			Config::get('chemin_modeles').'api_0.1'.DS);
 
-		foreach ($dossiers_classes as $chemin) {
-			$fichier_a_tester = $chemin.$nom_classe.'.php';
-			if (file_exists($fichier_a_tester)) {
-				include_once $fichier_a_tester;
-				return null;
-			}
-		}
-	}
-
 	public static function getNomDossierDepuisParametre($parametre) {
 		$dossier = str_replace('-', '_', strtolower($parametre));
 		return $dossier;
@@ -183,6 +167,7 @@
 
 	private static function executerModule() {
 		$classeModule = self::getNomClasseModule();
+
 		$action = self::getNomMethodeAction();
 		// Nous vérifions que le module existe
 		if (class_exists($classeModule)) {
Index: modules/fiche/Fiche.php
===================================================================
--- modules/fiche/Fiche.php	(révision 1137)
+++ modules/fiche/Fiche.php	(copie de travail)
@@ -16,7 +16,7 @@
 
 	private $onglet = 'synthese';
 	private $parametres = array();
-	private $conteneur = null;
+	public $conteneur = null;
 	private $num_nom = 0;
 	private $nom_retenu = '';
 	private $url;
Index: modules/fiche/formateurs/Bibliographie.php
===================================================================
--- modules/fiche/formateurs/Bibliographie.php	(révision 1137)
+++ modules/fiche/formateurs/Bibliographie.php	(copie de travail)
@@ -21,7 +21,7 @@
 	private $appUrls;
 	private $wikini;
 	private $referentiel = 'bdtfx';
-	private $donnees = array();
+	public $donnees = array();
 	
 	public function __construct(Conteneur $conteneur) {
 		$this->conteneur = $conteneur;
Index: modules/fiche/formateurs/Description.php
===================================================================
--- modules/fiche/formateurs/Description.php	(révision 1137)
+++ modules/fiche/formateurs/Description.php	(copie de travail)
@@ -26,7 +26,7 @@
 	private $CosteTexte;
 	
 	private $referentiel = 'bdtfx';
-	private $donnees = array();
+	public $donnees = array();
 
 	public function __construct(Conteneur $conteneur) {
 		$this->conteneur = $conteneur;
@@ -182,7 +182,7 @@
 		return $this->wikini->getUrlPageWiki($referentiel, $num_tax);
 	}
 
-	private function getCoste() {
+	public function getCoste() {
 		$coste = array();
 		$this->textes->setProjet('coste');
 		$this->textes->setId('bdtfx.nn:'.$this->nomCourant->getNnr());
Index: modules/fiche/formateurs/Ecologie.php
===================================================================
--- modules/fiche/formateurs/Ecologie.php	(révision 1137)
+++ modules/fiche/formateurs/Ecologie.php	(copie de travail)
@@ -18,7 +18,7 @@
 class Ecologie extends aControleur {
 	
 	private $referentiel = 'bdtfx';
-	private $donnees = array();
+	public $donnees = array();
 	
 	public function __construct(Conteneur $conteneur) {
 		$this->conteneur = $conteneur;
Index: modules/fiche/formateurs/Ethnobotanique.php
===================================================================
--- modules/fiche/formateurs/Ethnobotanique.php	(révision 1137)
+++ modules/fiche/formateurs/Ethnobotanique.php	(copie de travail)
@@ -19,7 +19,7 @@
 	private $nomsVernaculaires = null;
 	private $wikini;
 	private $referentiel = 'bdtfx';
-	private $donnees = array();
+	public $donnees = array();
 
 	public function __construct(Conteneur $conteneur) {
 		$this->conteneur = $conteneur;
@@ -113,12 +113,11 @@
 		// ignore les paramètres JSON de présence qui ne contiennent pas le libellé
 		// (= présence en toutes lettres)
 		if(substr($str, -7) != 'libelle') return NULL;
-		list($prefixe, $lieu) = explode('_', $str, 2);
-
 		if($str == 'presence.libelle') {
 			return $ref == 'bdtfx' ? 'France' : 'Antilles';
 		}
 
+		list($prefixe, $lieu) = explode('_', $str, 2);
 		list($lieu, $param) = explode('.', $lieu, 2);
 		if (strtolower($lieu) == 'ga') {
 			return 'France (Hors Corse)';
Index: modules/fiche/formateurs/Nomenclature.php
===================================================================
--- modules/fiche/formateurs/Nomenclature.php	(révision 1137)
+++ modules/fiche/formateurs/Nomenclature.php	(copie de travail)
@@ -19,7 +19,7 @@
 	private $noms = null;
 	private $meta = null;
 	private $referentiel = 'bdtfx';
-	private $donnees = array();
+	public $donnees = array();
 
 	public function __construct(Conteneur $conteneur) {
 		$this->conteneur = $conteneur;
