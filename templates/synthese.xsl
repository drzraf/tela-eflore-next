<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">

    <!-- TODO: ici: langue de la fiche -->
    <!--     <?xml version="1.0" encoding="UTF-8" lang="fr-fr" ?> -->
    <fiche>
      <description>
	<xsl:choose>
	  <xsl:when test="/fiche/description/coste/titre">
	    <xsl:copy-of select="/fiche/description/coste/*" />
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:choose>
	      <xsl:when test="/fiche/description/baseflor/titre">
		<xsl:copy-of select="/fiche/description/baseflor/*" />
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:copy-of select="/fiche/description/wiki/*" />
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:otherwise>
	</xsl:choose>
      </description>

      <xsl:copy-of select="/fiche/ethnobota" />
      <xsl:copy-of select="/fiche/ecologie" />
      <xsl:copy-of select="/fiche/classification" />
      <xsl:copy-of select="/fiche/biblio" />
      <!-- TODO: ici: URL de permalien du fichier XML de niveau 0 (original) -->

    </fiche>
  </xsl:template>
</xsl:stylesheet>

