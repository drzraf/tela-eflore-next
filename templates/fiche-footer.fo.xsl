<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/footer">
    <div class="gauche telechargements_pdf"></div>
    <div class="droite infos_liens_permanents"></div>
  </xsl:template>

</xsl:stylesheet>
