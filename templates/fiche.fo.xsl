<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="synthese.fo.xsl"/>
  <xsl:import href="fiche-footer.fo.xsl"/>

  <xsl:template match="/">
    <div class="importance4" id="bloc-fiche">
      <h1>
	<span class="nomenclature"><span class="sci"><span class="gen">Acer</span> <span class="sp">campestre</span></span> L.</span>
	<span style="float: right;" class="famille nomenclature">Sapindaceae </span><br />
	<span class="vernaculaire">Érable champêtre</span>
      </h1>
      
      <div class="ui-tabs ui-widget ui-widget-content ui-corner-all" id="zone_onglets">
	<!-- ... -->
      </div>

      <div id="zone_contenu_fiche">
	<!-- synthèse -->
      </div>
    </div>

    <div class="importance1">
      <!-- fiche-footer.fo.xsl -->
    </div>

  </xsl:template>
</xsl:stylesheet>
