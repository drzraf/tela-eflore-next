<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="ecologie-climat.xsl"/>
  <xsl:import href="ecologie-sol.xsl"/>


  <xsl:template match="/fiche/ecologie">
    <div id="ecologie">
      <?php if(isset($baseflor)) : ?>
      <h2>Optimum écologique</h2>
      <? if (!isset($baseflor['aucune'])) : ?>
      <div class="fond_graphique">
	<div class="bloc_graph">
	  <xsl:apply-templates select="/fiche/ecologie/phytosocio/climat"/>
	</div>

	<div class="bloc_graph">
	  <xsl:apply-templates select="/fiche/ecologie/phytosocio/sol"/>
	</div>
      </div>

      <div class="conteneur_lien_metadonnees">
	<?=$baseflor['meta']['citation']?> 
	<span class="conteneur_lien_metadonnees">
	  <a class="lien_metadonnees lien_popup " href="<?= $baseflor['meta']['url']; ?>">Voir toutes les metadonnées</a>
	</span>
      </div>

      <?else : ?> 
      <?=$baseflor['aucune']?> 
      <? endif; ?>
      <? endif; ?>
      <xsl:apply-templates select="/fiche/ecologie/catminat"/>

      <br/>
      <br/>

      <xsl:apply-templates select="/fiche/ecologie/complements"/>
    </div>
  </xsl:template>


  <xsl:template match="/fiche/ecologie/catminat">
    <h2>Phytosociologie</h2>
    <? if (isset($baseveg['syntaxons-sup']) || isset($baseveg['syntaxons-sup'])) : ?>
    <br/>
    Le code catminat de ce taxon est <span class="gras"> <?=$baseveg['baseflor']['catminat'] ?></span> <br/>
    Il est caractéristique du syntaxon de niveau <span class="gras"><?=$baseveg['syntaxon-courant'][0]['niveau.libelle']?></span> présenté 
    dans le tableau ci-dessous avec ses niveaux supérieurs. [ <a href="http://philippe.julve.pagesperso-orange.fr/catminat.htm" target="_blank">Voir le site Catminat pour en savoir plus.</a> ]
    <table>
      <?php foreach ($baseveg['syntaxons-sup'] as $cle => $valeurs ) : ?>
      <tr>
	<td>
	  <span class="gras"><?=$valeurs['code_catminat']?></span><br/>
	  <?=$valeurs['niveau.libelle']?> 
	  
	</td>
	<td>
	  <span class="gras"><?=$valeurs['syntaxon']?></span> <br/>
	  <span ><?=$valeurs['physio_biotype']?></span> <br/>
	  <?= !empty($valeurs['repartition_france']) ? 'répartition : '.$valeurs['repartition_france'] : '' ?>
	</td>
	<td>
	  
	  <a class="lien_popup" href="<?=$baseveg['ref']['lien-liste-fancy'][$valeurs['code_catminat']]?>">Réf.</a>  <br/>
	  <a class="lien_popup" href="<?=$baseveg['synonymes']['lien-liste-fancy'][$valeurs['code_catminat']]?>">Syn.</a> <br/>
	  <a  class="lien_popup" href="<?=$baseveg['taxons']['lien-liste-fancy'][$valeurs['code_catminat']]?>">Taxons</a>
	</td>
      </tr>
      <? endforeach; ?>
      <tr class="surlignage">
	<td> 
	  <span class="gras"><?=$baseveg['syntaxon-courant'][0]['code_catminat']?></span><br/>
	  <?=$baseveg['syntaxon-courant'][0]['niveau.libelle']?>
	  
	</td>
	<td>
	  <span class="gras"><?=$baseveg['syntaxon-courant'][0]['syntaxon']?></span> <br/>
	  <span ><?=$baseveg['syntaxon-courant'][0]['physio_biotype']?></span> <br/>
	  <?= !empty($baseveg['syntaxon-courant'][0]['repartition_france']) ? 'rép.'.$baseveg['syntaxon-courant'][0]['repartition_france'] : ''?>
	</td>
	<td>
	  <a class="lien_popup" href="<?=$baseveg['ref']['lien-liste-fancy'][$baseveg['syntaxon-courant'][0]['code_catminat']]?>">Réf.</a>  <br/>
	  <a class="lien_popup" href="<?=$baseveg['synonymes']['lien-liste-fancy'][$baseveg['syntaxon-courant'][0]['code_catminat']]?>">Syn.</a><br/>
	  <a  class="lien_popup" href="<?=$baseveg['baseflor']['lien-liste-fancy']?>">Taxons</a>
	  
	</td>
      </tr>
    </table>
    <div class="conteneur_lien_metadonnees">
      
      <?=$baseveg['meta']['citation']?> 
      <span class="conteneur_lien_metadonnees">
	<a class="lien_metadonnees lien_popup " href="<?= $baseveg['meta']['url']; ?>">Voir toutes les metadonnées</a>
      </span>
    </div>
    
    <? else : ?>
    Aucune donnée.
    <? endif; ?>
    <br/>
    <?php endif; ?>
  </xsl:template>

  <xsl:template match="/fiche/ecologie/complements">
    <h2>Vos compléments sur l'écologie</h2>
    <!-- TODO: cette phrase d'aide devrait être ajoutée avec javascript mais le système des onglets ajax nous en empêche -->
    <span class="aide_wikini"> Participez à la rédaction collaborative, un double clic dans le cadre suffit pour compléter ou corriger la page </span>
    <div class="contenu_editable">
      <div class="description wikini editable_sur_clic" title="ecologie">
	<?=$wikini['ecologie']?>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
