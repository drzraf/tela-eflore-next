<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/fiche/description/baseflor">
    <h2>Description Baseflor : </h2>
    <div class="description baseflor">
      <table class="desc">
	<tr>
	  <td colspan="2">
	    <?if ($baseflor['type_bio'] != '') : ?>
	    <span class="titre">Type Biologique : </span><?=$baseflor['type_bio']; ?> 
	    <? endif; ?>	
	  </td>
	</tr>

	<tr>
	  <td colspan="2">
	    <?if ($baseflor['form_vegetale'] != '') :?>
	    <span class="titre">Formation végétale : </span> <?=$baseflor['form_vegetale']; ?> 
	    <? endif; ?>
	  </td>
	</tr>

	<tr>
	  <td colspan="2">
	    <?if ($baseflor['chorologie'] != '') :?>
	    <span class="titre">Chorologie : </span>&nbsp<?=$baseflor['chorologie']; ?> 
	    <? endif; ?>
	  </td>
	</tr>

	<tr>
	  <td >		
	    <?if ($baseflor['inflorescence'] != '') :?>
	    <span class="titre">Inflorescence :</span> <?=$baseflor['inflorescence'];?> <br/>
	    <? endif; ?>
	    <?if ($baseflor['fruit'] != '') :?>
	    <span class="titre">Fruit :</span><?=$baseflor['fruit']; ?> <br/>
	    <? endif; ?>			
	    <?if ($baseflor['couleur_fleur'] != '') :?>
	    <span class="titre">Couleur de la fleur : </span><?=$baseflor['couleur_fleur'] ;?>  <br/>
	    <? endif; ?>
	    <?if ($baseflor['macule'] != '') :?>
	    <span class="titre">Macule : </span><?=$baseflor['macule']; ?> <br/>
	    <? endif; ?>		
	    <?if ($baseflor['floraison'] != '') :?>
	    <span class="titre">Floraison : </span><?=$baseflor['floraison']; ?> <br/>
	    <? endif; ?>
	  </td>
	  <td>	
	    <?if ($baseflor['sexualite'] != '') :?>
	    <span class="titre">sexualité : </span><?=$baseflor['sexualite']; ?> <br/>
	    <? endif; ?>
	    <?if ($baseflor['ordre_maturation'] != '') :?>
	    <span class="titre">Ordre de maturation : </span><?=$baseflor['ordre_maturation']; ?> <br/>
	    <? endif; ?>
	    <?if ($baseflor['pollinisation'] != '') :?>
	    <span class="titre">Pollinisation : </span><?=$baseflor['pollinisation']; ?> <br/>
	    <? endif; ?>
	    <?if ($baseflor['dissemination'] != '') :?>
	    <span class="titre">Dissémination : </span><?=$baseflor['dissemination'] ;?> <br/>
	    <? endif; ?>
	  </td>	
	</tr>		
      </table>	
      
      <div class="conteneur_lien_metadonnees">
	<?=$baseflor['meta']['citation']?> 
	
	<span class="conteneur_lien_metadonnees">
	  <a class="lien_metadonnees lien_popup " href="<?= $baseflor['meta']['url']; ?>">Voir toutes les metadonnées</a>
	</span>
      </div>
    </div>

  </xsl:template>
</xsl:stylesheet>
