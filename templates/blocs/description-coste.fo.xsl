<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/fiche/description/coste">
    <h2>Description de Coste : <?=$coste['titre']?></h2>
    <div class="description coste">
      <? if (!empty($coste['description'])) : ?>
      <table class="desc">

	<tr>
	  <td>
	    <?if ($coste['description']['nom_scientifique'] != '') : ?>
	    <span class="gras"> 
	      <?=$coste['description']['nom_scientifique']; ?>
	    </span> 
	    <? endif; ?>
	    <?if ($coste['description']['nom_commun'] != '') : ?>
	    <span class="italique">
	      <?=$coste['description']['nom_commun']; ?>
	    </span>
	    
	    <? endif; ?>
	  </td>
	</tr>

	<?if ($coste['description']['synonymes'] != '') : ?>
	<tr>
	  <td>
	    <span class="titre">Synonymes </span>
	    <?=$coste['description']['synonymes']; ?> 
	  </td>
	</tr>
	<? endif; ?>

	<tr>
	  <td>
	    <?if ($coste['description']['texte'] != '') : ?>
	    <?=$coste['description']['texte']; ?> 
	    <br/>
	    <? endif; ?>
	  </td>
	</tr>

	<?if ($coste['description']['ecologie'] != '') : ?>
	<tr>
	  <td>
	    <span class="titre">Écologie </span>
	    <?=$coste['description']['ecologie']; ?>
	  </td>
	</tr>
	<? endif; ?>

	<?if ($coste['description']['repartition'] != '') : ?>
	<tr>
	  <td>
	    <span class="titre">Répartition </span>
	    <?=$coste['description']['repartition']; ?> 
	  </td>
	</tr>
	<? endif; ?>

	<?if ($coste['description']['floraison'] != '') : ?>				
	<tr>
	  <td>
	    <span class="titre">Floraison </span>
	    <?=$coste['description']['floraison']; ?>
	    <?if ($coste['description']['fructification'] != '') : ?>
	    <span class="titre">Fructification </span>
	    <?=$coste['description']['fructification']; ?>
	    <? endif; ?>
	  </td>
	</tr>
	<? endif; ?>

	<?if ($coste['description']['usages'] != '') : ?>						
	<tr>
	  <td>
	    <span class="titre">Usages</span>
	    <?=$coste['description']['usages']; ?>
	  </td>
	</tr>
	<? endif; ?>
      </table>
      
      <div class="conteneur_lien_metadonnees">
	<?=$coste['meta']['citation']?>
	<span class="conteneur_lien_metadonnees">
	  <a class="lien_metadonnees lien_popup" href="<?= $coste['meta']['url']; ?>">Voir toutes les metadonnées</a>
	</span>
      </div>
      <? else : ?>
      Aucune donnée.
      <? endif; ?>
    </div>
  
  </xsl:template>
</xsl:stylesheet>
