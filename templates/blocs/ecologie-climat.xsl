<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="/services/presentations/images/graphiques/climat.svg" />
  <xsl:import href="/services/presentations/images/graphiques/climat_min_max.svg" />

  <xsl:template match="/fiche/ecologie/phytosocio/climat">
    <? if (isset($baseflor['climat_url'])) :?>
    <span class="titre">caractéristiques climatiques</span>
    
    <div class="graphique">
      <object class="ecologie_svg" type="image/svg+xml" data="<?=$baseflor['climat_url']?>" class="svg" alt="<?=$baseflor['climat_url_png']?>" />
    </div>
    
    <xsl:apply-templates select="/legende"/>
  </xsl:template>

  <xsl:template match="/legende">
    <button class="voir"> voir la légende </button>
    <button class="cacher">masquer la légende </button>
    <br/>
    <div class="legende_graphique">
      <table>
	<tr> 
	  <td class="largeur-02" >
	    <span class="titre">Lumière </span>
	  </td>
	  <td>
	    <? foreach ($baseflor['legende']['VEL'] as $code => $infos ) : ?>
	    <?=$code ?> :  <?=$infos['nom'] ?> 
	    <? endforeach;	?>
	  </td>
	</tr>
	<tr> 
	  <td>
	    <span class="titre">Température </span>
	  </td>
	  <td>
	    <? foreach ($baseflor['legende']['VET'] as $code => $infos ) : ?>
	    <?=$code ?> :  <?=$infos['nom'] ?> 
	    <? endforeach;	?>
	  </td>
	</tr>
	<tr> 
	  <td>
	    <span class="titre">Humidité atmosphérique </span>
	  </td>
	  <td>
	    <? foreach ($baseflor['legende']['VEHA'] as $code => $infos ) : ?>
	    <?=$code ?> :  <?=$infos['nom'] ?> 
	    <? endforeach;	?>
	  </td>
	</tr>
	<tr> 
	  <td>
	    <span class="titre">Continentalité </span>
	  </td>
	  <td>
	    <? foreach ($baseflor['legende']['VEC'] as $code => $infos ) : ?>
	    <?=$code ?> :  <?=$infos['nom'] ?> 
	    <? endforeach;	?>
	  </td>
	</tr>
      </table>
    </div>
  </xsl:template>

</xsl:stylesheet>
