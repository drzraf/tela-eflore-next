<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/fiche/description/wikipedia">
    <h2>Fiche wikipedia : <?=$wp['titre']?></h2>
    <div class="description wp">
      <a class="lien_externe" href="<?=htmlentities($wp['lien'])?>" title="voir la page de ce taxon sur wikipedia (s'ouvre dans une nouvelle fenêtre)">
      <?=htmlentities($wp['lien'])?>
    </a>
  </xsl:template>
</xsl:stylesheet>
