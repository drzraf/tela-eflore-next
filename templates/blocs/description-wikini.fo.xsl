<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/fiche/description/wikini">
    <h2> Description collaborative : </h2>
    <!-- TODO: cette phrase d'aide devrait être ajoutée avec javascript mais le système des onglets ajax nous en empêche -->
    <div class="description wikini">
      <span class="aide_wikini"> Participez à la rédaction collaborative de cette description, un double clic dans le cadre suffit pour compléter ou corriger la page </span>
      <div class="contenu_editable">
	<div class="description wikini editable_sur_clic" title="description">
	  <?=$wikini['description']?>
	</div>
      </div>
    </div>

  </xsl:template>
</xsl:stylesheet>
