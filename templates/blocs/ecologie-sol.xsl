<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="/services/presentations/images/graphiques/sol.svg" />
  <xsl:import href="/services/presentations/images/graphiques/sol_min_max.svg" />

  <xsl:template match="/fiche/ecologie/phytosocio/sol">

    <span class="titre">caractéristiques du sol</span>
    
    <div class="graphique">
      <object class="ecologie_svg" type="image/svg+xml" data="<?=$baseflor['sol_url']?>" class="svg" alt="<?=$baseflor['sol_url_png']?>" />
    </div>

    <xsl:apply-templates select="/legende"/>
  </xsl:template>

  <xsl:template match="/legende">
    <button class="voir"> voir la légende </button>
    <button class="cacher">masquer la légende </button>
    <br/>
    <div class="legende_graphique">
      <table>
	<tr> 
	  <td class="largeur-02">
	    <span class="titre"> Réaction (pH)  </span>
	  </td>
	  <td>
	    <? foreach ($baseflor['legende']['VER'] as $code => $infos ) : ?>
	    <?=$code ?> :  <?=$infos['nom'] ?> 
	    <? endforeach;	?>
	  </td>
	</tr>
	<tr> 
	  <td>
	    <span class="titre"> Humidité   </span>
	  </td>
	  <td>
	    <? foreach ($baseflor['legende']['VEHE'] as $code => $infos ) : ?>
	    <?=$code ?> :  <?=$infos['nom'] ?> 
	    <? endforeach;	?>
	  </td>
	</tr>
	<tr> 
	  <td>
	    <span class="titre"> Texture   </span>
	  </td>
	  <td>
	    <? foreach ($baseflor['legende']['VETX'] as $code => $infos ) : ?>
	    <?=$code ?> :  <?=$infos['nom'] ?> 
	    <? endforeach;	?>
	  </td>
	</tr>
	<tr> 
	  <td>
	    <span class="titre"> Nutriments   </span>
	  </td>
	  <td>
	    <? foreach ($baseflor['legende']['VEN'] as $code => $infos ) : ?>
	    <?=$code ?> :  <?=$infos['nom'] ?> 
	    <? endforeach;	?>
	  </td>
	</tr>
	<tr> 
	  <td>
	    <span class="titre">  Salinité   </span>
	  </td>
	  <td>
	    <? foreach ($baseflor['legende']['VES'] as $code => $infos ) : ?>
	    <?=$code ?> :  <?=$infos['nom'] ?> 
	    <? endforeach;	?>
	  </td>
	</tr>
	<tr> 
	  <td>
	    <span class="titre"> Matière Organique   </span>
	    
	  </td>
	  <td>
	    <? foreach ($baseflor['legende']['VEMO'] as $code => $infos ) : ?>
	    <?=$code ?> :  <?=$infos['nom'] ?> 
	    <? endforeach;	?>
	  </td>
	</tr>
      </table>	
      
    </div>

  </xsl:template>
</xsl:stylesheet>
