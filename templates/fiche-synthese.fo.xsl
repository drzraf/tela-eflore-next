<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="fiche-footer.fo.xsl"/>

  <!-- synthèse -->
  <xsl:template match="/fiche">
    <div class="synthese">
      <div class="colonne deuxtiers ui-sortable">
	<xsl:apply-templates select="/fiche/description"/>
	<xsl:apply-templates select="/fiche/ethnobotanique"/>
	<xsl:apply-templates select="/fiche/ecologie"/>
	<xsl:apply-templates select="/fiche/nomenclature"/>
	<xsl:apply-templates select="/fiche/bibliographie"/>
      </div>
      <div class="colonne ui-sortable">
	<xsl:apply-templates select="/fiche/illustrations"/>
	<xsl:apply-templates select="/fiche/repartition"/>
	<xsl:apply-templates select="/fiche/status-de-protection"/>
      </div>
    </div>
  </xsl:template>




  <xsl:template match="/fiche/description">
    <!-- description -->
    <div id="synthese_description" class="module ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
      <h3 class="titre ui-widget-header ui-corner-all" id="titre-1">
	<a class="titreOnglet lien-onglet-synthese" href="#2">Description</a>
      <span class="lienToggle ui-icon ui-icon-minusthick"></span></h3>
      <div id="contenu-1" class="contenu">
	<xsl:value-of select="coste"/>
	<xsl:value-of select="@url"/>
	<xsl:value-of select="coste/@url"/>
	<xsl:value-of select="metadata/@url"/>

	<h4>Description collaborative</h4>
	<h3> Générale </h3>
	Mettez ici la description générale !
	<h3> Identification </h3>
	Mettez ici, ce qui permet d'identifier la plante !
	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=description" class="lien_ouverture_onglet_parent" id="alignement-droite">
	  Voir +/Compléter
	</a>
      </div>
    </div>
  </xsl:template>




  <xsl:template match="/fiche/ethnobotanique">
    <div id="synthese_ethnobotanique" class="module ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
      <h3 class="titre ui-widget-header ui-corner-all" id="titre-4">
	<a class="titreOnglet lien-onglet-synthese" href="#3">Ethnobotanique</a>
      <span class="lienToggle ui-icon ui-icon-minusthick"></span></h3>
      <div id="contenu-4" class="contenu">
	<h4>Noms communs</h4>
	Aucun nom vernaculaire
	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=ethnobotanique" class="lien_ouverture_onglet_parent">
	  Rajouter vos informations.
	</a>
	<h4>Usages</h4>
	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=ethnobotanique" class="lien_ouverture_onglet_parent">
	  Rajouter vos informations.
	</a>

	<h4>Culture et arts</h4>
	<xsl:element name="a">
          <xsl:attribute name="href">
	    ?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=ethnobotanique
	    &amp;num_nom=<xsl:value-of select="nn"/>
	  </xsl:attribute>
	  <xsl:attribute name="class">
	    lien_ouverture_onglet_parent
	  </xsl:attribute>
	  Rajouter vos informations.
	</xsl:element>

	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=ethnobotanique" class="lien_ouverture_onglet_parent" id="alignement-droite">
	  Voir +/Compléter
	</a>
      </div>
    </div>
  </xsl:template>




  <xsl:template match="/fiche/ecologie">
    <div id="synthese_ecologie" class="module ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
      <h3 class="titre ui-widget-header ui-corner-all" id="titre-8">
	<a class="titreOnglet lien-onglet-synthese" href="#4">Ecologie</a>
      <span class="lienToggle ui-icon ui-icon-minusthick"></span></h3>
      <div id="contenu-8" class="contenu">
	<h4>Optimum écologique</h4>
	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=statut" class="lien_ouverture_onglet_parent">
	  Rajouter vos informations.
	</a>
	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=ecologie" class="lien_ouverture_onglet_parent" id="alignement-droite">
	  Voir +/Compléter
	</a>
      </div>
    </div>
  </xsl:template>



  <xsl:template match="/fiche/nomenclature">
    <div id="synthese_nomenclature" class="module ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
      <h3 class="titre ui-widget-header ui-corner-all" id="titre-6">
	<a class="titreOnglet lien-onglet-synthese" href="#5">Classification</a>
      <span class="lienToggle ui-icon ui-icon-minusthick"></span></h3>
      <div id="contenu-6" class="contenu">
	<h4>Taxons supérieurs</h4>
	
	<div>Aucun <span rel="taxon" class="definition_term">taxon</span> supérieur référencé </div>
	<h4>Nom retenu</h4>
	<div>
	  <div class="nom retenu surlignage">
	  </div>
	</div>
	
	<h4>Synonymes</h4>
	Aucun synonyme
	<h4>Taxons inférieurs</h4>
	
	<div>Aucun <span rel="taxon" class="definition_term">taxon</span> inférieur référencé </div>
	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=nomenclature" class="lien_ouverture_onglet_parent" id="alignement-droite">
	  Voir +/Compléter
	</a>
      </div>
    </div>
  </xsl:template>



  <xsl:template match="/fiche/bibliographie">
    <div id="synthese_bibliographie" class="module ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
      <h3 class="titre ui-widget-header ui-corner-all" id="titre-2">
	<a class="titreOnglet lien-onglet-synthese" href="#6">Bibliographie</a>
      <span class="lienToggle ui-icon ui-icon-minusthick"></span></h3>
      <div id="contenu-2" class="contenu">
	<h4>Flores</h4>
	Aucune flore indiquée
	<h4>Références</h4>
	<div> - Mais quelle est donc cette plante ? </div>
	<div> - Notes de lecture </div>
	<div> - Index des <span rel="article" class="definition_term">articles</span> parus dans le volume I de Richardiana </div>
	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=nomenclature" class="lien_ouverture_onglet_parent" id="alignement-droite">
	et 7autresréférences</a>
	<hr class="nettoyage" />
	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=bibliographie" class="lien_ouverture_onglet_parent" id="alignement-droite">
	  Voir +/Compléter
	</a>
      </div>
    </div>
  </xsl:template>




  <xsl:template match="/fiche/illustrations">
    <div id="synthese_illustrations" class="module ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
      <h3 class="titre ui-widget-header ui-corner-all" id="titre-0">
	<a class="titreOnglet lien-onglet-synthese" href="#7">Illustrations</a>
      <span class="lienToggle ui-icon ui-icon-minusthick"></span></h3>
      <div id="contenu-0" class="contenu">
	<span class="absent" style="float:none; margin-left:55px">
	  Pas de photo<br/>
	  <a href="http://www.tela-botanica.org/page:cel" title="Ajouter une photographie de  au moyen du Carnet en Ligne" onclick="window.open(this.href); return false;" class="contribuer">
	    Contribuer
	  </a>
	</span>
	<br/>
	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=illustrations" class="lien_ouverture_onglet_parent" id="alignement-droite">
	  Voir +/Compléter
	</a>
      </div>
    </div>
  </xsl:template>


  <xsl:template match="/fiche/repartition">
    <div id="synthese_repartition" class="module ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
      <h3 class="titre ui-widget-header ui-corner-all" id="titre-3">
	<a class="titreOnglet lien-onglet-synthese" href="#8">Répartition</a>
      <span class="lienToggle ui-icon ui-icon-minusthick"></span></h3>
      <div id="contenu-3" class="contenu">
	<h4>Observations</h4>
	<img src="http://www.tela-botanica.org/service:eflore:0.1/moissonnage/cartes?referentiel=bdtfx&amp;num_taxon=&amp;format=190&amp;retour=image%2Fpng" alt="Carte des observations" style="margin-left: 11px; width: 190px;" />
	<h4>Répartition départementale</h4>
	<img src="http://www.tela-botanica.org/service:eflore:0.1/chorodep/cartes/nn%3A?retour.format=190x178&amp;retour=image%2Fpng" alt="Carte de répartition" />
	<br/>
	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=repartition" class="lien_ouverture_onglet_parent" id="alignement-droite">
	  Voir +/Compléter
	</a>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="/fiche/status-de-protection">
    <div id="synthese_statut" class="module ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
      <h3 class="titre ui-widget-header ui-corner-all" id="titre-7">
	<a class="titreOnglet lien-onglet-synthese" href="#9">Protection</a>
      <span class="lienToggle ui-icon ui-icon-minusthick"></span></h3>
      <div id="contenu-7" class="contenu ">
	<span class="pas_de_protection">Pas de protection connue</span>
	<br/>
	<a href="?referentiel=bdtfx&amp;niveau=2&amp;module=fiche&amp;num_nom=199999999941&amp;type_nom=nom_scientifique&amp;nom=acer&amp;onglet=statut" class="lien_ouverture_onglet_parent" id="alignement-droite">
	  Voir +/Compléter
	</a>
      </div>
    </div>
  </xsl:template>

</xsl:stylesheet>
