<?php

if(!@$argv[1]) die(<<<EOF
Syntax: php conf-deobfuscator.php [other ini-file to load, ...] <ini-file to deobfuscate> [framework.php path]
Resolves {ref:} php:<> and path-structure (x.y) in .ini files then output the human-readable directives.

EOF
);

array_shift($argv);
$inifiles = [];
$frameworkpath = 'framework.php';

foreach($argv as $v) {
  if(preg_match('/framework.*php/', $v)) { $frameworkpath = $v; break; }
  $inifiles[] = $v;
}
require_once $frameworkpath;
Framework::setCheminAppli($frameworkpath);
Framework::setInfoAppli(Config::get('info'));

if(!Config::$parametres) die('cant touch Config::$parametres, framework needs patching [private => public]');

for($i = 0; $i < count($inifiles) - 1; $i++) { 
  Config::charger($inifiles[$i]);
}
$savedConf = Config::$parametres;
Config::charger($inifiles[$i]);
//var_dump(Config::$parametres);die;
$my_conf = @array_diff_assoc(Config::$parametres, $savedConf);

set_include_path(get_include_path() . PATH_SEPARATOR . getenv('HOME'). '/pear/php');
require_once('Config/Lite.php');
$config = new Config_Lite();
// $config->write('/tmp/new/' . str_replace('/', '-', $inifiles[$i]) . '.parsed', $my_conf);
$config->write("/dev/stdout", $my_conf);
