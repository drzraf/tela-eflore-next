<?php
// declare(encoding='UTF-8');
/**
 * Config permet de charger automatiquement les fichiers ini du Framework et de l'application.
 * Elle offre l'accès en lecture seule aux paramètres de config.
 * C'est une Singleton.
 * Si vous avez besoin de modifier dynamiquement des paramètres de configuration, utiliser le @see Registe, il est fait pour ça.
 *
 * @category	PHP 5.2
 * @package	Framework
 * @author		Jean-Pascal MILCENT <jpm@tela-botanica.org>
 * @copyright	Copyright (c) 2009, Tela Botanica (accueil@tela-botanica.org)
 * @license	http://www.gnu.org/licenses/gpl.html Licence GNU-GPL-v3
 * @license	http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt Licence CECILL-v2
 * @version	$Id: Config.php 402 2011-12-29 10:44:54Z jpm $
 * @link		/doc/framework/
 */

class Config {

	/** Instance de la classe pointant sur elle même (pour le pattern singleton). */
	private static $instance = null;

	/** Paramètres de configuration. */
	public static $parametres = array();

	private function __construct() {
		// Définition de paramètres avant chargement du config.ini
		self::$parametres = array(
			'fichier_config' => 'config%s.ini',
			'chemin_framework' => dirname(__FILE__).DS
			);

		// Chargement du fichier config.ini du Framework
		$existe = self::parserFichierIni(self::$parametres['chemin_framework'].sprintf(self::$parametres['fichier_config'], ''));
		if ($existe === false) {
			trigger_error("Veuillez configurer le Framework en renommant le fichier config.defaut.ini en config.ini.", E_USER_ERROR);
		}

		// Chargement du fichier config.ini par défaut de l'application
		$chemin_config_defaut_appli = self::$parametres['chemin_configurations'].sprintf(self::$parametres['fichier_config'], '');
		self::parserFichierIni($chemin_config_defaut_appli);

		// Chargement des fichiers config.ini contextuels
		if (PHP_SAPI == 'cli') {// mode console
			foreach ($_SERVER['argv'] as $cle => $valeur) {
				if ($valeur == '-contexte') {
					self::chargerFichierContexte($_SERVER['argv'][($cle+1)]);
					break;
				}
			}
		} else {// mode web
			// Pour Papyrus
			if (defined('PAP_VERSION')) {
				self::chargerFichierContexte('papyrus');
			}
			// Via le fichie .ini par défaut de l'appli
			if (Config::existeValeur('info.contexte', self::$parametres)) {
				self::chargerFichierContexte(Config::get('info.contexte'));
			}

			// Chargement du contexte présent dans le GET
			if (isset($_GET['contexte'])) {
				$_GET['contexte'] = strip_tags($_GET['contexte']);
				self::chargerFichierContexte($_GET['contexte']);
			}

			// Chargement du contexte présent dans le POST
			if (isset($_POST['contexte'])) {
				$_POST['contexte'] = strip_tags($_POST['contexte']);
				self::chargerFichierContexte($_POST['contexte']);
			}
		}
	}

	/**
	 * Charge le fichier de config correspondant au contexte
	 * @param string $contexte le contexte
	 */
	private static function chargerFichierContexte($contexte) {
		$chemin_config_appli_contextuel = self::$parametres['chemin_configurations'];
		$chemin_config_appli_contextuel .= sprintf(self::$parametres['fichier_config'], '_'.$contexte);
		self::parserFichierIni($chemin_config_appli_contextuel);
	}

	/**
	 * Parse le fichier ini donné en paramètre
	 * @param string $fichier_ini nom du fichier ini à parser
	 * @return array tableau contenant les paramètres du fichier ini
	 */
	private static function parserFichierIni($fichier_ini) {
		$retour = false;
		$ini = parse_ini_file($fichier_ini, true);
		$ini = self::analyserTableauIni($ini);
		return true;
	}

	/**
	 * Renvoie la valeur demandée grâce une chaîne de paramètres
	 * @param string $param la chaine de paramètres
	 * @param array $config le tableau de paramètre
	 * @return string la valeur de la chaine demandée
	 */
	private static function getValeur($param, $config) {
		if ($param === null) {
			return null;
		} else {
			if (isset($config[$param])) {
				return $config[$param];
			} else if (strpos($param, '.') !== false) {
				$pieces = explode('.', $param, 2);
				if (strlen($pieces[0]) && strlen($pieces[1])) {
					if (isset($config[$pieces[0]])) {
					   if (is_array($config[$pieces[0]])) {
					   		return self::getValeur($pieces[1], $config[$pieces[0]]);
					   }
					}
				}
			} else {
				return null;
			}
		}
	}

	/**
	 * Teste si param existe dans le tableau config
	 * @param string $param nom du paramètre
	 * @param array tableau de configuration
	 */
	private static function existeValeur($param, $config) {
		$retour = false;
		if (self::getValeur($param, $config) !== null) {
			$retour = true;
		}
		return $retour;
	}

	/**
	 * Vérifie si l'instance de classe à été crée, si non la crée
	 */
	private static function verifierCreationInstance() {
		if (empty(self::$instance)) {
			self::$instance = new Config();
		}
	}



	private static function analyserTableauIni($config = array()) {
	  self::$parametres = array_merge(self::$parametres, $config);
	  array_walk_recursive(self::$parametres,
			       function(&$item, $key, $fullconf) {
				 $item = self::_getstr($item, $fullconf);
			       },
			       self::$parametres);

	  foreach (self::$parametres as $cle => $valeur) {
	    if (!is_array($valeur)) self::evaluerCle(self::$parametres, $cle, $valeur);
	  }

	  return self::$parametres;
	}

	private static function _getstr($str = array(), $fullconf) {
	  // évaluer php
	  if (preg_match('/^php:(.+)$/', $str, $correspondances)) {
	    $x = eval('return '. self::_getstr($correspondances[1].';', $fullconf));
	    return is_bool($x) ? (int)$x : $x;
	  }

	  // évaluer références
	  if (preg_match_all('/{ref:([A-Za-z0-9_.-]+)}/', $str, $correspondances, PREG_SET_ORDER)) {
	    foreach ($correspondances as $ref) {
	      if(array_key_exists($ref[1], $fullconf))
		$str = str_replace($ref[0], self::_getstr($fullconf[$ref[1]], $fullconf), $str);
	      // évaluer les sous-indices de tableau indiqués comme {ref:}
	      elseif(strpos($ref[1], '.') !== false) {
		$temp = $fullconf;
		foreach(explode('.', $ref[1]) as $key) $temp = &$temp[$key];
		if(@$temp) $str = str_replace($ref[0], self::_getstr($temp, $fullconf), $str);
	      }

	    }
	    return $str;
	  }

	  return $str;
	}

	/**
	 * Dans le cas des chaine de configuration à sous clé (ex.: cle.souscle)
	 * évalue les valeurs correspondantes et crée les sous tableaux associés.
	 * @param array $config tableau de configuration (par référence)
	 * @param string $cle la cle dans le tableau
	 * @param string $valeur la valeur à affecter
	 */
	private static function evaluerCle(&$config, $cle, $valeur) {
	  if(strpos($cle, '.') === false) return $config;
	  unset($config[$cle]);
	  @list($nkey, $nval) = explode('.', $cle, 2);
	  if (!$nkey || !$nval) {
	    trigger_error("Clé invalide '$cle'", E_USER_WARNING);
	    return;
	  }

	  if (array_key_exists($nkey, $config) && /* why !is_array ? */ !is_array($config[$nkey])) {
	    trigger_error("Ne peut pas créer de sous-clé pour '{$nkey}' car la clé existe déjà", E_USER_WARNING);
	    return;
	  }

	  $config[$nkey][$nval] = $valeur;
	  foreach($config[$nkey] as $k => $v) self::evaluerCle($config[$nkey], $k, $v);
	}

	/**
	 * Charge un fichier ini dans le tableau des paramètres de l'appli.
	 * @param string $fichier_ini le nom du fichier à charger
	 * @return array le fichier ini parsé
	 */
	public static function charger($fichier_ini) {
		self::verifierCreationInstance();
		return self::parserFichierIni($fichier_ini);
	}

	/**
	 * Accesseur pour la valeur d'un paramètre.
	 * @param string $param le nom du paramètre
	 * @return string la valeur du paramètre
	 */
	public static function get($param = null) {
		self::verifierCreationInstance();
		return self::getValeur($param, self::$parametres);
	}

	/**
	 * Vérifie si la valeur d'un paramètre existe.
	 * @param string $param le nom du paramètre
	 * @return boolean vrai si le paramètre existe, false sinon
	 */
	public static function existe($param) {
		self::verifierCreationInstance();
		return self::existeValeur($param, self::$parametres);
	}

	/**
	 * Vérifie que tous les paramêtres de config nécessaires au fonctionnement d'une classe existe dans les fichiers
	 * de configurations.
	 * L'utilisation de cette méthode depuis la classe Config évite de faire appel à une classe supplémentaire.
	 *
	 * @param array $parametres tableau des noms des paramètres de la config à verifier.
	 * @return boolean true si tous les paramétres sont présents sinon false.
	 */
	public static function verifierPresenceParametres(Array $parametres) {
		$ok = true;
		foreach ($parametres as $param) {
			if (is_null(self::get($param))) {
				$classe = get_class();
				$m = "L'utilisation de la classe $classe nécessite de définir '$param' dans un fichier de configuration.";
				trigger_error($m, E_USER_ERROR);
				$ok = false;
			}
		}
		return $ok;
	}
}
?>