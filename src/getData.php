<?php

include('Constants.php');

if(! @$argv[1]) die('arg: num_nom');
$db = new PDO('mysql:dbname=ref', 'telabotap', NULL, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "UTF8"'));

$dom = new DomDocument();
$dom->formatOutput = TRUE;
$dom->preserveWhiteSpace = false; 
$dom->load(__DIR__ . '/../template.xml');

// symlink tables:
// http://dba.stackexchange.com/questions/10108/can-i-specify-a-different-disk-for-a-specific-mysql-table


$main = $db->query(sprintf('SELECT * FROM bdtfx_v2_00 WHERE num_nom = %d', intval($argv[1])))
           ->fetch(PDO::FETCH_ASSOC);
$nomret = $db->query(sprintf('SELECT nom_sci AS nom_retenu FROM bdtfx_v2_00 WHERE num_nom = %d', $main['num_nom_retenu']))
             ->fetch(PDO::FETCH_ASSOC);
$synonyms = $db->query(sprintf('SELECT num_nom, CONCAT(nom_sci, " ", auteur) AS synonyme FROM bdtfx_v2_00 WHERE num_nom_retenu = %d AND num_nom != num_nom_retenu', $main['num_nom']))
               ->fetchAll(PDO::FETCH_KEY_PAIR);
$img_coste = $db->query(sprintf('SELECT image FROM coste_v2_00 WHERE flore_bdtfx_nt = %d', $main['num_taxonomique']))
                ->fetch(PDO::FETCH_ASSOC);

// projets/services/modules/0.1/coste/Textes.php:220
/* $data_coste = $db->query(sprintf('SELECT c.nom_sci_html AS titre,dsc.body AS texte,cle.body AS determination,dsc.tag AS tag,c.num_nom AS "coste_num" FROM tb_eflore.coste_v2_00 c LEFT JOIN tela_prod_wikini.florecoste_pages dsc ON c.page_wiki_dsc = dsc.tag AND dsc.latest = "Y" LEFT JOIN tela_prod_wikini.florecoste_pages cle ON c.page_wiki_cle = cle.tag AND cle.latest = "Y" WHERE c.flore_bdtfx_nn = %d', $main['num_nom_retenu']))
   ->fetch(PDO::FETCH_ASSOC); */


$rec = array_merge($main, $nomret, ['synonymes' => $synonyms]);
sqlFieldRename($rec);



// substitutions simples (ajout de l'attribut value="")
alterDomSimpleDeep($dom, $rec);

// substitutions de constantes dans les attributs "url"
$constants = [
    '{wikiAPIURL}' => $dom->getElementsByTagName('wikiAPIURL')->item(0)->nodeValue,
    '{APIURL}' => $dom->getElementsByTagName('APIURL')->item(0)->nodeValue,
];
foreach($rec as $k => $v) $constants['{' . $k . '}'] = $v;
alterDomConstantsDeep($dom, $constants);

// ajout des synonymes
$xpath = new DOMXpath($dom);
$synoNode = $xpath->query("/fiche/classification/synonymes")->item(0);
foreach($synonyms as $nn => $v) {
    $sNode = $dom->createElement("s");
    $sNode->setAttribute("value", $v);
    $sNode->setAttribute("nn", $nn);
    $synoNode->appendChild($sNode);
}


// ajout des images de coste
$costeNode = $xpath->query("/fiche/illustrations/coste")->item(0);
$costeNode->setAttribute("value", COSTE_BASEURL . $img_coste['image']);

// ajout de la description de coste
$costeNode = $xpath->query("/fiche/description/coste")->item(0);
$costeNode->appendChild($dom->createElement("titre", $data_coste['titre']));
$costeNode->appendChild($dom->createElement("texte", $data_coste['texte']));
$costeNode->appendChild($dom->createElement("determination", $data_coste['determination']));
$costeNode->appendChild($dom->createElement("tag", $data_coste['tag']));
$costeNode->appendChild($dom->createElement("coste_num", $data_coste['coste_num']));


$refNode = $xpath->query("/fiche/classification/referentiel")->item(0);
$refNode->setAttribute("code", "bdtfx");
$refNode->setAttribute("version", "2.00");
$refNode->setAttribute("label", "Trachéophytes...");


// ajout de la biblio
$biblioNode = $xpath->query("/fiche/biblio/flores")->item(0);
$biblioNode->appendChild($dom->createElement(
    "value",
    /* \\fiche\formateurs\Bibliographie::getCorrespondancesBiblio(explode(",", $rec["flores"])) */ NULL));

$destdir = sprintf('fiches/xml/%s/%d/%d.xml',
		   $xpath->query("/fiche/classification/referentiel")->item(0)->getAttribute('code'),
		   substr($xpath->query("/fiche/classification/referentiel/nn")->item(0)->getAttribute('value'),0, 1),
		   $xpath->query("/fiche/classification/referentiel/nn")->item(0)->getAttribute('value')) ;
/* ou bien
$destdir = sprintf('fiches/xml/%s/%s/%d/%d.xml',
		   $xpath->query("/fiche/classification/referentiel/@code"),
		   $xpath->query("/fiche/classification/referentiel/@version"),
		   substr($xpath->query("/fiche/classification/nn"),0, 1)) ;
car:
10 versions de 5 référentiels de 500 000 taxons =
= 25 000 000 files  = 250 Go pour des fichiers de 10 ko
largement manageable (+ éventuellement queqlues options de montage ext4)
*/

echo "==> to $destdir";
echo $dom->saveXML();


function alterDomSimpleDeep(DOMNode $domNode, $vals) {
    foreach ($domNode->childNodes as $node) {
        if(array_key_exists($node->nodeName, $vals) && !is_array($vals[$node->nodeName])) {
            // more complex possibilities:
            $node->setAttribute("value", $vals[$node->nodeName]);
        }
        if($node->hasChildNodes()) alterDomSimpleDeep($node, $vals);
    }
}


function alterDomConstantsDeep(DOMNode $domNode, $constants) {
    foreach ($domNode->childNodes as $node) {
        if($node->nodeType == XML_ELEMENT_NODE && ($url = $node->getAttribute("url"))) {
            $node->setAttribute("url", replaceConstants($url, $constants));
        }
        if($node->hasChildNodes()) alterDomConstantsDeep($node, $constants);
    }
}

function replaceConstants($url, $constants) {
    return preg_replace_callback(
        '/(\{\w+\})/',
        function($i) use($constants) {
            return @$constants[$i[0]] ?: $i[0];
        },
        $url);
}


function sqlFieldRename(&$rec) {
    $rec['nn'] = $rec['num_nom'];
    $rec['nt'] = $rec['num_taxonomique'] ?: $rec['num_tax'];
}




/*
 * - définir si les constantes d'URL de hostname relèvent du XML ou du générateur PHP
 * - définir si les paramètres de format d'URL relèvent du XML ou non (cf img chorodep, content-type de wiki, ...)
 * - webservice chorodep: paramètre "withSynonyms"
 * - DublinCore
 * - XSL/PHP func binding: http://stackoverflow.com/questions/4649537/
 */